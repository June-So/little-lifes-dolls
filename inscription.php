<?php include 'includes/header.php' ?>
<?php
  /* A REVOIR - S'active aprés l'envoi du formulaire Inscription,
  Verifie que le formulaire est correct et si c'est le cas
  ajoute les données dans la table 'membres'; */
  if($_POST){
    //Vérification de la validité des informations, et que le mdp et que la confirmation est pareil
    if(verifRegex('/^[a-zA-Z\s-]+$/','pseudo') && verifRegex('/[^<>]/','mdp') && ($_POST['mdp'] == $_POST['confirmMdp'])){
      //Hachage du mot de pass avec sha1();
      echo 'Inscription réusssi';
      $mdp_hache = sha1($_POST['mdp']);
      //Insertion dans la base de donnée
      $DB->insert('INSERT INTO membres(`pseudo`,mdp,date_inscription) VALUES(:pseudo,:mdp, CURDATE())',array(
      'pseudo' => $_POST['pseudo'],
      'mdp' => $mdp_hache));
    } else {
      echo 'Votre inscription a échoué';
    }
  }
?>
  <!-- Verfier le formulaire de façon dynamique avec js:
        - L'utilisateur doit pouvoir voir ses erreurs pendant la saisie
        - Prévenir l'utilisateur si le pseudo n'est pas déjà pris-->
<section class="row">
  <div class="col-lg-offset-2">
    <h2>Inscription</h2>
    <form name="inscriptionForm" action="inscription.php" method="post">
      <label for="pseudo">Choisissez un pseudo</label><br/>
      <input type="text" name="pseudo"/><br/>
      <label for="mdp">Choisissez un mot de passe</label><br/>
      <input type="password" name="mdp"/><br/>
      <label for="confirmMdp">Confirmez votre mot de passe</label><br/>
      <input type="password" name="confirmMdp"/><br/>
      <input class="boutonMenu" type="submit" value="S'incrire"/>
    </form>
    <p>Déjà inscrit?  <a href="/index.php">Connectez-vous</a></p>
  </div>
</section>

<?php include 'includes/footer.php' ?>
