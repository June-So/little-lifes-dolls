<!-- Redimmensionner les images avant  -->
<?php include 'includes/header.php';
$Preview = new Preview($DB);
if(!$_SESSION){
  header('Location: index.php');
}?>
<div class="zone-preview">

  <?php
  //Affichage Preview etapes
  ?>

</div> <!-- fin de la partie du preview du projet -->
<section>
  <!-- Barre de donnée, choix de l'affichage de la base de donnée -->
<?php
  $ids = array_keys($_SESSION['etapes']);
  $formes = $DB->query('SELECT * FROM formes WHERE id IN ('.implode(',',$ids).')');
  foreach($formes as $forme){
    echo $forme->id.' '.$forme->titre;
  }
?>
  <!-- Zone Affichage bibliotheque des formes -->
  <?php
  var_dump($_SESSION);
    $reponse = $DB->query('SELECT * FROM formes');
    foreach($reponse as $donnees){
      ?>
      <div class="col-lg-3 seeDiv">
      <div class="row">
        <h2 class="col-lg-12"> <?php echo $donnees->titre.' '.$donnees->difficulte; ?></h2>
      </div>
        <div class="row">
          <img src="<?php echo $donnees->image; ?>" class="col-lg-6"/>
          <div class="col-lg-6">
            <p>Taille Crochet: X<br/>
              Taille de la laine:<br/>
              Couleur primaire:<br/>
              Ajouter une couleur: <button>+</button>
            </p>
            <p><?php echo $donnees->auteur; ?> - <a href="<?php echo $donnees->lien_auteur; ?>"><?php echo $donnees->source_pattern ?></a></p>
             <a href="fonctions/addpreview.php?id=<?= $donnees->id; ?>"><button class="addPreview"> Ajouter </button></a>
            <!-- Le bouton doit etre un lien vers une action avec ?parametre

            Quand on clicque sur le bouton ajouter,
                 Envoi les valeurs vers un tableau preview[]-->
          </div>
        </div>
      </div>
      <?php
    }
  ?>
</section> <!-- fin zone bibliotheque -->
<?php include 'includes/footer.php' ?>
