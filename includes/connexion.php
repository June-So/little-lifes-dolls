<?php
//Zone de connexion si le post est envoyé,
if($_POST && !$_SESSION){
//Hachage du mot de passe
$mdp_hache = sha1($_POST['mdp']);

//Vérification des identifiants
$resultat = $DB->query('SELECT id FROM membres WHERE BINARY pseudo = :pseudo AND mdp = :mdp',array(
  'pseudo' => $_POST['pseudo'],
  'mdp' => $mdp_hache));
  //$req-> bindValue('pseudo', $_POST['pseudo'],PDO::PARAM_STR);

//Si la requête est valide ci dessus alors démarre une session
if ($resultat) {
  session_start();
  $_SESSION['id'] = $resultat['id'];
  $_SESSION['pseudo'] = $_POST['pseudo'];
  echo 'Vous êtes connecté !';
  header('Location:/index.php');
} else {
  echo 'Mauvais identifiants ou mot de passe !';
}
}
?>
  <h2>Connectez-vous</h2>
  <form name="connexion" method="post" action="index.php">
    <label for="pseudo">Pseudo</label><br/>
    <input type="text" name="pseudo"/><br/>
    <label for="mdp">Mot de passe</label><br/>
    <input type="password" name="mdp"/><br/>
    <input class="boutonMenu" type="submit" value="Se connecter"/>
  </form>
  <p>Pas encore inscrit ? <a href="inscription.php">S'enregistrer</a>
