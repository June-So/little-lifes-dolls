<?php
session_start();
require 'class/db.class.php';
require 'class/preview.class.php';
$DB = new DB();
 ?>
 <!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Accueil - Little Dolls</title>
    <link rel="stylesheet" href="/assets/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/assets/font-awesome-4.6.3/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/css/modif-bootstrap.css"/>
    <link rel="stylesheet" href="/css/style.css"/>
  </head>
  <body ng-app="crochetApp">
    <div class="container-fluid">
      <header>
        <h1 class="titreSite col-lg-11">Little Dolls Life</h1>
        <p><?php if($_SESSION){ echo $_SESSION['pseudo']; }?>
        <a href="/fonctions/deconnexion.php"><button class="boutonConnexion"><i class="fa fa-power-off fa-2x" aria-hidden="true"></i></button></a></p>
      </form>
      </header>
<?php
      // function (regex,$_POST['___']) si le $_POST éxiste -> Vérifier si le $_POST suit la regle du regex
      function verifRegex($pattern,$post){
        if(isset($_POST[$post])){
          if(preg_match($pattern,$_POST[$post])){ //Si(verifie que le pattern suit la regex)
            return true;
            } else { //Sinon Affiche un message d'erreur et retourne faux
            return false;
            }
        } else { //Sinon (si le post n'éxiste pas) retourne false
            return 'Error';
          }
      }
      ?>
