<?php include 'includes/header.php' ?>
      <section class="row">
        <div class="col-lg-2">
          <?php if($_SESSION){ ?>
          <ul>
            <li><a href=bibliotheque.php><button class="boutonMenu">BIBLIOTHEQUE</button></a></li>
            <li><a href=#><button class="boutonMenu">Plan de travail</button></a></li>
          </ul>
          <?php } ?>
        </div><!-- fin du menu gauche-->
        <div class="col-lg-offset-2 col-lg-4">
          <!-- Montre le formulaire de connexion si il n'y a pas de session en cours -->
          <?php if(!$_SESSION){ include 'includes/connexion.php'; } ?>
        </div><!-- fin partie gauche  -->

      </section>
<?php include 'includes/footer.php' ?>
