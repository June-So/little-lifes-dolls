<?php
class DB{
  // Paramétres par défaut
  private $host = 'localhost';
  private $username = 'dollys';
  private $password = 'kupo';
  private $database = 'littleDolls';
  private $db;


  //Construction avec paramétres
  public function __construct($host = null, $username = null, $password = null, $database = null){
    if($host != null){
      $this->host = $host;
      $this->username = $username;
      $this->password = $password;
      $this->database = $database;
    }
    //connexion
    try{
    $this->db = new PDO('mysql:host='.$this->host.';dbname='.$this->database.';charset=utf8',$this->username,$this->password,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    }catch(Exception $e){
      die('Erreur de connexion à la base'. $e->getMessage());
    }
  }

  //Methode de requête d'affichage
  public function query($sql,$data = array()){
    $req = $this->db->prepare($sql);
    $req->execute($data);
    return $req->fetchAll(PDO::FETCH_OBJ);
  }

  //Méthode de requête d'insertion
  public function insert($sql,$data = array()){
    $request = $this->db->prepare($sql);
    $request->execute($data);
  }
}
?>
